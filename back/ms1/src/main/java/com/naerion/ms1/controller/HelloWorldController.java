package com.naerion.ms1.controller;

import com.naerion.ms1.data.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author gbogaert
 *
 */
@RestController
@RequestMapping("hello")
public class HelloWorldController {

	private static final String HELLO_WORLD_FROM = "Hello World from ";

	@Value("${spring.application.name}")
	private String appName;
	
	@Value("${service.host}")
	private String serviceHost;

	@Autowired
	RestTemplate restTemplate;

	@GetMapping
	public ResponseEntity<Response> hello() {
		String helloString = HELLO_WORLD_FROM + appName + " !! ";
		return ResponseEntity.ok(new Response(helloString));
	}

	@GetMapping(path = "/ms2")
	public ResponseEntity<Response> helloChain() {
		String helloString = HELLO_WORLD_FROM + appName + " !! ";
		return ResponseEntity
				.ok(new Response(helloString + restTemplate.getForEntity("http://"+serviceHost+":8080/hello", Response.class).getBody().getMessage()));
	}
}
