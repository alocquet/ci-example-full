import {HttpClient, HttpClientModule} from "@angular/common/http";
import { Microservice2Service } from './service/microservice2.service';
import { Microservice1Service } from './service/microservice1.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler, Injectable } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule} from '@angular/material/button';
import {MatGridListModule} from '@angular/material/grid-list';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';


import * as Sentry from "@sentry/browser";

Sentry.init({
  dsn: "https://c7ef08ff21ec4c73bd8afbc1d6e1aaef@sentry.io/1524017"
});

@Injectable()
export class SentryErrorHandler implements ErrorHandler {
  constructor() {}
  handleError(error) {
    const eventId = Sentry.captureException(error.originalError || error);
    Sentry.showReportDialog({ eventId });
  }
}

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatButtonModule,
    BrowserAnimationsModule,
    MatGridListModule, HttpClientModule
  ],
  providers: [Microservice1Service, Microservice2Service, { provide: ErrorHandler, useClass: SentryErrorHandler }],
  bootstrap: [AppComponent]
})
export class AppModule { 
}
