import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { Location, PlatformLocation } from "@angular/common";
@Injectable()
export class Microservice2Service {
  url: string = "/api/ms2/hello";

  constructor(private http: HttpClient) {}

  getHello(): Observable<any> {
    return this.http.get(this.url);
  }

  getHelloFeign(): Observable<any> {
    return this.http.get(this.url + "/ms1");
  }
}
