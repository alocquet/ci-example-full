import { Microservice2Service } from "./service/microservice2.service";
import { Microservice1Service } from "./service/microservice1.service";
import { Component } from "@angular/core";
import { Observable } from "rxjs";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  title = "portal";
  callMs1Observable: Observable<any>;
  callMs2Observable: Observable<any>;
  callMs1FeignObservable: Observable<any>;
  callMs2FeignObservable: Observable<any>;

  constructor(private ms1Service: Microservice1Service,
    private ms2Service: Microservice2Service
  ) {}

  public callMs1() {
    this.callMs1Observable = this.ms1Service.getHello();
  }

  public callMs2() {
    this.callMs2Observable = this.ms2Service.getHello();
  }

  public callMs1Feign() {
    this.callMs1FeignObservable = this.ms1Service.getHelloFeign();
  }

  public callMs2Feign() {
    this.callMs2FeignObservable = this.ms2Service.getHelloFeign();
  }
}
